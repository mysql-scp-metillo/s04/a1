-- Instruction A: Find all artists with the letter d in their name.
SELECT * FROM artists WHERE name LIKE "%d%";

-- Instruction B: Find all songs that have a length < 2:30.
SELECT * FROM songs WHERE length < 230;

-- Instruction C: Join the albums and songs tables
-- Only show the album name, song name, and song length
SELECT albums.album_title, songs.song_name, songs.length FROM albums
	JOIN songs ON albums.id = songs.album_id;

-- Instruction D: Join the artists and albums tables. (Find all albums that have a letter A in its name)
SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id WHERE albums.album_title LIKE "%a%";

-- Instruction E: Sort albums in Z-A order. (Show only the first 4 records)
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- Instruction F :Join the albums and songs tables. (Sort albums from Z-A)
SELECT * FROM albums
	JOIN songs ON albums.id = songs.album_id
	ORDER BY albums.album_title DESC;
